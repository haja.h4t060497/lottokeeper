import { createStore } from 'redux'
import allReducers from './redux/reducers'

const store = createStore(
  allReducers,
  window.__REDUX_DEVTOOLS_EXTENSION_ && window.__REDUX_DEVTOOLS_EXTENSION_(),
)
export default store