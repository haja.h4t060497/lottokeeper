const setPlayerData = (playerName, balance) => {
    localStorage.setItem('playerName', playerName);
    localStorage.setItem('balance', balance);
  };
  
  export { setPlayerData };