import React from "react";
import { Button as RButton } from "react-bootstrap";
import styled from "styled-components";

const Button = ({
  text = "",
  // textcolor = "#FFF",
  radius = 50,
  fontSize = 18,
  uppercase = false,
  pv = 21,
  ph = 30,
  ...rest
}) => {
  return (
    <CustomButton
      // textcolor={textcolor}
      radius={radius}
      fontSize={fontSize}
      uppercase={uppercase ? "uppercase" : "inherit"}
      pv={pv}
      ph={ph}
      {...rest}
    >
      {text}
    </CustomButton>
  );
};

const CustomButton = styled(RButton)`
  border-radius: ${(p) => p?.radius}px;
  text-transform: ${(p) => p?.uppercase};
  font-weight: 200;
  font-size: ${(p) => p?.fontSize}px;
  line-height: 20px;
  padding: ${(p) => p?.pv}px ${(p) => p?.ph}px;
  letter-spacing: 0.1em;
  margin-right: 20px;
  /* color: ${(p) => p.textcolor}; */
`;

export default Button;
