import logo from './logo.svg';
import './App.css';
import Lotto from './pages/Lotto';


function App() {
  return (
    <div className="App">
        <Lotto/>
    </div>
  );
}

export default App;
