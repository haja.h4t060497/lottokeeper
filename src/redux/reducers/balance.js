const balanceReducer = (
    state = { balance: 0 },
    { type, ...rest }
) => {
    switch (type) {
        case "set":
            return { ...state, ...rest };
        default:
            return state;
    }
};
export default balanceReducer;