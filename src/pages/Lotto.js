import React,{useState,  useEffect} from 'react'
import Button from '../components/Button';
import { setPlayerData } from '../localStorageUtils';

export const Lotto = () => {
    const [playerName, setPlayerName] = useState(localStorage.getItem('playerName') ? localStorage.getItem('playerName') : 'Haja');
    const [balance, setBalance] = useState(localStorage.getItem('balance') ? localStorage.getItem('balance') : 10000);
    const [slips, setSlips] = useState([]);
    const [selectedNumbers, setSelectedNumbers] = useState([]);
    const [winningNumbers, setWinningNumbers] = useState([]);
    const [result, setResult] = useState(null);
    const [coins, setCoins] = useState(0);
    const [init , setInit] = useState(false)
    const [hasFrame, setHasFrame] = useState(false); // Vérifie si le joueur a le cadre nécessaire
    const [section, setSection] = useState(null);

    //section
    const [sectionsPlayers, setSectionsPlayers] = useState([]);
    const [numberOfSections, setNumberOfSections] = useState(1); // Number of sections
    const [operatorBalance, setOperatorBalance] = useState(0);

    const [hits, setHits] = useState();
    const [payment, setPayement] = useState();

  
const handleNumberSelect = (number) => {
    if (selectedNumbers.includes(number)) {
      setSelectedNumbers(selectedNumbers.filter((n) => n !== number));
    } else {
      if (selectedNumbers.length < 5) {
        setSelectedNumbers([...selectedNumbers, number]);
      }
    }

    addSlip(selectedNumbers, hits);
  };

  console.log("playerName", playerName)
  console.log("number", selectedNumbers)
  // result
  const generateWinningNumbers = () => {
    const randomNumbers = [];
    while (randomNumbers.length < 5) {
      const randomNumber = Math.floor(Math.random() * 39) + 1;
      if (!randomNumbers.includes(randomNumber)) {
        randomNumbers.push(randomNumber);
      }
    }
    setWinningNumbers(randomNumbers.sort((a, b) => a - b));
    setInit(true)
    
  };
  useEffect(()=> {
    if(init){
        checkResults();

        sortSlipsByHits();
        handleUpdateData();
        generateSection();
        
    }
  },[winningNumbers])

  const checkResults = () => {
    const hits = selectedNumbers.filter((num) => winningNumbers.includes(num)).length;
    // const hits = selectedNumbers.filter((num) => winningNumbers.includes(num));
    console.log(hits , 'length')
    let prize = 0;

    switch (hits) {
      case 2:
        prize = 100;
        break;
      case 3:
        prize = 500;
        break;
      case 4:
        prize = 1000;
        break;
      case 5:
        prize = 5000;
        break;
      default:
        break;
    }

   
    setResult(`ticket : You've got ${hits} hits! Prize: ${prize} coins`);
    setBalance(balance + prize - 500);
    setCoins(coins + prize - 500); // Deduct 500 coins for playing

    //drawing
    setHits(hits)
    setPayement(prize);
   
  };

  const handleUpdateData = () => {
    // Update the state
    setPlayerName('Haja');
    // setBalance(5000);

    // Save to localStorage using the utility function
    if(localStorage.getItem('playerName') && localStorage.getItem('balance')){
        localStorage.removeItem('playerName');
        localStorage.removeItem('balance');
        setPlayerData(playerName, balance);
    }
    else {
        setPlayerData(playerName, balance);
    }
  };


//   useEffect(() => {
//     const savedName = localStorage.getItem('playerName');
//     if (savedName) {
//       setPlayerName(savedName);
//     }

//     const savedBalance = localStorage.getItem('balance');
//     if (savedBalance) {
//       setBalance(Number(savedBalance));
//     }
//   }, []);

  useEffect(()=> {
    handleUpdateData()
  },[balance])

  // Function to add a slip based on guesses
  const addSlip = (guesses, hits) => {
    const newSlip = {
      guesses: guesses,
      hits: hits,
    };
    // setSlips([...slips, newSlip]);
    setSlips([newSlip]);
    console.log(slips)
  };

  console.log("hits", hits)
  // Render slips without allowing sorting
  const renderSlips = () => {
    return slips.map((slip, index) => (
      <div key={index}>
        <p> your guesses: {slip.guesses.join(', ')}</p>
        <p>Hits: {slip.hits}</p>
      </div>
    ));
  };

  const generateSection = () => {
    
    let hasRequiredFrame = null; // Condition pour vérifier le cadre (à remplacer par votre logique)
    if(playerName !== ""){
        hasRequiredFrame = true
    }
    else{
        hasRequiredFrame = false
    }

    if (hasRequiredFrame) {
      // Générer la section
      const newSection = {
        playerName: playerName,
        balance: balance,
      };
      // Mettre à jour la section générée
      setSection(newSection);
      setHasFrame(true);
    } else {
      alert('Insufficient frame to generate section!');
    }
  };

  const renderSections = () => {
    return (
      <div>
        <p>your Section:</p>
        <h3>Section: Hello Player {section.playerName}, your balance {section.balance}</h3>  
        <hr/>
      </div>
    );
  };
   // Sort slips based on hits
   const sortSlipsByHits = () => {
    const sortedSlips = [...slips].sort((a, b) => b.hits - a.hits); // Sort in descending order
    setSlips(sortedSlips);
  };

//   const generateSlip = () => {
//     // Implement logic to generate a slip with selected numbers
//   };

 // Function to generate random numbers for a player section
 const generateRandomNumbers = () => {
    const randomNumbers = Array.from({ length: 5 }, () => Math.floor(Math.random() * 39) + 1);
    return randomNumbers;
  };

  const simulatePlayers = () => {
  
    const newSections = [];

    const pricePerCoupon = 500; // Price of a coupon
    const totalPrice = numberOfSections * pricePerCoupon;

    // Update operator's balance by adding the total price of generated coupons
    setOperatorBalance(operatorBalance + totalPrice);

    for (let i = 0; i < numberOfSections; i++) {
      const section = {
        playerName: `Player ${i + 1}`,
        numbers: generateRandomNumbers(), // Generate random numbers for each player
      };
      newSections.push(section);
    }

    setSectionsPlayers(newSections);
    generateWinningNumbers()
  };

  // Render players sections
  const renderSectionsPlayers = () => {
    return sectionsPlayers.map((section, index) => (
      <div key={index}>
        <h3>{section.playerName}</h3>
        <p>Numbers: {section.numbers.join(', ')}</p>
        <div>

      <p>Your Coins: {coins}</p>
      {result && <p>{result}</p>}
      {winningNumbers.length > 0 && <p>Winning Numbers: {winningNumbers.join(', ')}</p>}
    </div>
      </div>
    ));
  };



  return (
    <div>
    <h1>Lottery Game</h1>
    
    
    <div>
    <label htmlFor="sectionInput">Enter number of sections:</label>
      <input
        id="sectionInput"
        type="number"
        value={numberOfSections}
        onChange={(e) => setNumberOfSections(Number(e.target.value))}
      />   
    <button onClick={() => simulatePlayers()}>Simulate Players</button>
    <p>Operator's Balance: {operatorBalance} credits</p>
      {sectionsPlayers.length > 0 && (
        <div>
          <h2>Simulated Players:</h2>
          {renderSectionsPlayers()}
        </div>
      )}
    </div>
    <hr/>
    {playerName !== " " ?(
        <div>
        <form onSubmit={(e) => {
        e.preventDefault();
        handleUpdateData();
    }}>
        <label className='me-2'>your informations</label>
    <input
        type="text"
        value={playerName}
        onChange={(e) => setPlayerName(e.target.value)}
    />
    <input
        type="number"
        value={balance}
        onChange={(e) => setBalance(e.target.value)}
    />
    <button type="submit">Save</button>
    </form>
    </div>
    ) : (<div><h3>Hi {playerName}</h3></div>)}
    {section !==null && (
        <div>
            {renderSections()}
        </div>
    )}
    <div>
      <h2>Select 5 Numbers (1-39):</h2>
      {[...Array(39)].map((_, index) => (
        <button
          key={index + 1}
          onClick={() => handleNumberSelect(index + 1)}
          disabled={selectedNumbers.length === 5 && !selectedNumbers.includes(index + 1)}
          style={{
            backgroundColor: selectedNumbers.includes(index + 1) ? 'yellow' : 'white',
          }}
        >
          {index + 1}
        </button>
      ))}
    </div>
    <div>
      <button onClick={generateWinningNumbers}>Generate Winning Numbers</button>
      <p>Your Coins: {coins}</p>
      <p>Your balance: {balance}</p>
      {result && <p>{result}</p>}
      {winningNumbers.length > 0 && <p>Winning Numbers: {winningNumbers.join(', ')}</p>}
    </div>
    <div>
    {renderSlips()}
    </div>
    <div>
    {hits > 0 && (
        <div>
          <p>Number of Hits: {hits}</p>
          <p>Payment: {payment} coins</p>
        </div>
      )}
    </div>
  </div>

  )
}

export default Lotto;